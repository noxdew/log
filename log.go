package log

import (
	zap "go.uber.org/zap"
	zapcore "go.uber.org/zap/zapcore"
)

// Copied from the original zap.Config, everything made a pointer so it can be nil
type zapOverwriteableConfig struct {
	Level             *zap.AtomicLevel        `json:"level" yaml:"level"`
	Development       *bool                   `json:"development" yaml:"development"`
	DisableCaller     *bool                   `json:"disableCaller" yaml:"disableCaller"`
	DisableStacktrace *bool                   `json:"disableStacktrace" yaml:"disableStacktrace"`
	Sampling          *zap.SamplingConfig     `json:"sampling" yaml:"sampling"`
	Encoding          *string                 `json:"encoding" yaml:"encoding"`
	EncoderConfig     *zapcore.EncoderConfig  `json:"encoderConfig" yaml:"encoderConfig"`
	OutputPaths       *[]string               `json:"outputPaths" yaml:"outputPaths"`
	ErrorOutputPaths  *[]string               `json:"errorOutputPaths" yaml:"errorOutputPaths"`
	InitialFields     *map[string]interface{} `json:"initialFields" yaml:"initialFields"`
}

// Config represents the configuration of the logger
type Config struct {
	Type      string                 `json:"type" yaml:"type"`
	ZapConfig zapOverwriteableConfig `json:"config" yaml:"config"`
}

// Create Creates a logger from the config. It will panic on all errors as lack of
// valid logger configuration will mean the service is running in the dark
func (config Config) Create() *zap.Logger {
	var zapConfig zap.Config
	if config.Type == "structured" {
		zapConfig = zap.NewProductionConfig()
	} else if config.Type == "unstructured" {
		zapConfig = zap.NewDevelopmentConfig()
	} else {
		zap.L().Panic("Invalid logging type",
			zap.Strings("valid", []string{"structured", "unstructured"}),
			zap.String("type", config.Type),
		)
	}

	configOverwrites := config.ZapConfig
	if configOverwrites.Level != nil {
		zapConfig.Level = *configOverwrites.Level
	}
	if configOverwrites.Development != nil {
		zapConfig.Development = *configOverwrites.Development
	}
	if configOverwrites.DisableCaller != nil {
		zapConfig.DisableCaller = *configOverwrites.DisableCaller
	}
	if configOverwrites.DisableStacktrace != nil {
		zapConfig.DisableStacktrace = *configOverwrites.DisableStacktrace
	}
	if configOverwrites.Sampling != nil {
		zapConfig.Sampling = configOverwrites.Sampling
	}
	if configOverwrites.Encoding != nil {
		zapConfig.Encoding = *configOverwrites.Encoding
	}
	if configOverwrites.OutputPaths != nil {
		zapConfig.OutputPaths = *configOverwrites.OutputPaths
	}
	if configOverwrites.ErrorOutputPaths != nil {
		zapConfig.ErrorOutputPaths = *configOverwrites.ErrorOutputPaths
	}
	if configOverwrites.InitialFields != nil {
		zapConfig.InitialFields = *configOverwrites.InitialFields
	}
	if configOverwrites.EncoderConfig != nil {
		zapConfig.EncoderConfig = *configOverwrites.EncoderConfig
	} else if config.Type == "unstructured" {
		zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	}

	logger, err := zapConfig.Build()
	if err != nil {
		zap.L().Panic("Failed to create logger",
			zap.String("type", config.Type),
			zap.Error(err),
		)
	}

	zap.ReplaceGlobals(logger)
	zap.RedirectStdLogAt(logger, zap.WarnLevel)

	return logger
}
